import numpy as np


def window_stack(a):
    width = max(int(a.shape[0] / 10), 2)
    a = np.hstack([np.zeros((width - 1)), a, np.zeros((width - 1))])
    tmpList = list([a[i: i + width or None: 1] for i in range(0, a.shape[0] - 1)])
    meanList = []
    for tmp in tmpList:
        if len(tmp) == width:
            nonzero_index = np.nonzero(tmp)
            meanList.append(tmp[nonzero_index[0][0]: (nonzero_index[0][-1] + 1)].mean())
    return np.hstack(np.array(meanList))