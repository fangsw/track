# coding=utf-8

import csv
import numpy as np


def calcIOU(box1, box2):
    one_x, one_y, one_w, one_h = box1
    two_x, two_y, two_w, two_h = box2

    one_x, one_y, one_w, one_h = float(one_x), float(one_y), float(one_w), float(one_h)
    two_x, two_y, two_w, two_h = float(two_x), float(two_y), float(two_w), float(two_h)

    if (abs(one_x - two_x) < ((one_w + two_w) / 2.0)) and (abs(one_y - two_y) < ((one_h + two_h) / 2.0)):
        lu_x_inter = max((one_x - (one_w / 2.0)), (two_x - (two_w / 2.0)))
        lu_y_inter = min((one_y + (one_h / 2.0)), (two_y + (two_h / 2.0)))

        rd_x_inter = min((one_x + (one_w / 2.0)), (two_x + (two_w / 2.0)))
        rd_y_inter = max((one_y - (one_h / 2.0)), (two_y - (two_h / 2.0)))

        inter_w = abs(rd_x_inter - lu_x_inter)
        inter_h = abs(lu_y_inter - rd_y_inter)

        inter_square = inter_w * inter_h
        union_square = (one_w * one_h) + (two_w * two_h) - inter_square

        calcIOU = inter_square / union_square * 1.0
        # print("calcIOU:", calcIOU)
    else:
        calcIOU = 0

    return calcIOU


def readCVS(path):
    results = []
    with open(path) as f:
        f_csv = csv.reader(f)
        next(f_csv)
        for i, row in enumerate(f_csv):
            # if (i + 1) % 10 == 0:
            #     break
            frame_num = row[0]
            list = row[1:]
            num = round(len(list) / 6)
            row_list = []
            for i in range(num):
                start = i * 6
                end = start + 6
                sublist = list[start: end]
                if sublist[0] is not "":
                    sublist.append(frame_num)
                    row_list.append(sublist)
            results.append(row_list)
    return results


def boxTake(list):
    return list[2:6]


def filterTrack(list, pixel_item):
    results = []
    for track in list:
        start = track[0]
        end = track[-1]
        start_x = float(start[2])
        start_y = float(start[3])
        end_x = float(end[2])
        end_y = float(end[3])
        if abs(start_x - end_x) > pixel_item or abs(start_y - end_y) > pixel_item:
            results.append(track)
    return results


if __name__ == "__main__":
    FRAME_THRESHOLD = 30
    IOU_THRESHOLD = 0.5
    TAKE_FRAME_INTERVAL = 5

    data_list = readCVS("boxFile.csv")
    frist_line = data_list[0]
    tail_line = data_list[1:]
    track_list = []
    
    for data in frist_line:
        track_list.append([data])

    for i, line in enumerate(tail_line[::TAKE_FRAME_INTERVAL]):
        print("process frame: {}".format(i + 1 + TAKE_FRAME_INTERVAL))
        
        for bbox_info in line:
            box = boxTake(bbox_info)
            if float(box[0]) > 0 and float(box[1]) > 0 and float(box[2]) > 0 and float(box[3]) > 0:
                frame_num = int(bbox_info[-1])
                iou_list = []
                frame_interval_list = []

                for t in track_list:
                    last_track_box = boxTake(t[-1])
                    last_track_frame_num = int(t[-1][-1])
                    iou = calcIOU(box, last_track_box)
                    iou_list.append(iou)
                    frame_interval_list.append(abs(frame_num - last_track_frame_num))

                iou_listToIndex = iou_list.copy()
                iou_list.sort()
                max_iou = iou_list[-1]
                # print("max_iou: {}".format(max_iou))
                if max_iou > IOU_THRESHOLD:
                    index = iou_listToIndex.index(max_iou)
                    frame_interval = frame_interval_list[index]
                    if frame_interval < FRAME_THRESHOLD:
                        track_list[index].append(bbox_info)
                    else:
                        track_list.append([bbox_info])
                else:
                    track_list.append([bbox_info])
    # print(track_list)

    # 过滤移动像素x轴方向和y轴方向小于 50 的轨迹
    filterTrack(track_list, 50)

    track_array = np.asarray(track_list)
    print(track_array.shape)
    np.save("trackFile.npy", track_array)
    a = np.load("trackFile.npy")
    print(a[1])

    # with open("trackFile.txt", "w") as f:
    #     for track in track_list:
    #         f.write(str(track))
    #         f.write("\n")
