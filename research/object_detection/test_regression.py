import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
from scipy.stats import norm
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LinearRegression, Ridge, Lasso
from sklearn.preprocessing import PolynomialFeatures
from sklearn import linear_model
from sklearn.preprocessing import scale

''' 数据生成 '''


# x = np.arange(0, 1, 0.002)
# y = norm.rvs(0, size=500, scale=0.1)
# y += x ** 2

x = np.array([1166., 1169., 1172., 1176., 1182., 1183., 1188.
                 , 1188., 1190., 1190., 1193., 1193., 1193., 1193., 1195., 1196., 1195.
                 , 1197., 1197., 1198., 1198., 1197., 1199., 1200., 1203., 1200., 1202.
                 , 1203., 1202., 1203., 1203., 1205., 1205., 1205., 1205., 1207., 1206.
                 , 1208., 1207., 1207., 1209., 1211., 1213., 1214., 1216., 1219., 1223.
                 , 1224., 1227., 1230., 1234., 1238., 1243., 1246., 1253., 1259., 1266.
                 , 1271., 1277., 1285., 1295., 1301., 1310., 1319., 1330., 1338., 1347.
                 , 1353., 1368., 1378., 1393., 1406., 1421., 1428., 1449., 1465., 1477.
                 , 1490., 1513., 1528.])
x = scale(x)
y = np.array([1060., 1039., 1001., 974., 946., 928., 900.
                 , 883., 861., 846., 820., 805., 788., 776., 760., 747., 734.
                 , 725., 712., 701., 691., 688., 681., 675., 660., 655., 645.
                 , 641., 630., 625., 619., 614., 612., 598., 595., 590., 583.
                 , 578., 569., 565., 557., 554., 550., 546., 539., 536., 530.
                 , 525., 520., 516., 510., 507., 508., 502., 495., 494., 486.
                 , 486., 479., 474., 473., 471., 468., 464., 460., 456., 450.
                 , 448., 444., 445., 445., 444., 442., 439., 433., 434., 433.
                 , 428., 426., 423.])
y = scale(y)
x1 = window_stack(x)
y1 = window_stack(y)


# 均方误差根
def rmse(y_test, y):
    return sp.sqrt(sp.mean((y_test - y) ** 2))


def R2(y_test, y_true):
    return 1 - ((y_test - y_true) ** 2).sum() / ((y_true - y_true.mean()) ** 2).sum()


def R22(y_test, y_true):
    y_mean = np.array(y_true)
    y_mean[:] = y_mean.mean()
    return 1 - rmse(y_test, y_true) / rmse(y_mean, y_true)


degree = [1, 2, 3, 5]
y_test = []
y_test = np.array(y_test)

for d in degree:
    plt.plot(x1, y1)
    clf = Pipeline([('poly', PolynomialFeatures(degree=d)), ('linear', LinearRegression(fit_intercept=True, normalize=True))])
    # clf = Pipeline([('poly', PolynomialFeatures(degree=d)), ('linear', Ridge(alpha=10, fit_intercept=True, normalize=True))])
    clf.fit(x1[:, np.newaxis], y1)
    y_test = clf.predict(x1[:, np.newaxis])

    print(clf.named_steps['linear'].coef_)
    print('rmse=%.2f, R2=%.2f, R22=%.2f, clf.score=%.2f' % (
        rmse(y_test, y1), R2(y_test, y1), R22(y_test, y1), clf.score(x1[:, np.newaxis], y1)))

    plt.plot(x1, y_test, linewidth=2)

    plt.grid()
    plt.show()
