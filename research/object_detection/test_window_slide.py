# import numpy as np
# from numpy import linalg as la
#
#
# # 欧式距离
# def euclidSimilar(inA, inB):
#     return 2 * (1.0 / (1.0 + la.norm(inA - inB)) - 0.5)
#
#
# # 皮尔逊相关系数
# def pearsonSimilar(inA, inB):
#     return np.corrcoef(inA, inB, rowvar=0)[0][1] * 0.5 + 0.5
#
#
# # 余弦相似度
# def cosSimilar(inA, inB):
#     inA = np.mat(inA)
#     inB = np.mat(inB)
#     num = float(inA * inB.T)
#     denom = la.norm(inA) * la.norm(inB)
#     return 0.5 + 0.5 * (num / denom)
#
#
# x, y, z, k, m = np.array(
#     [116.543625731, 91.0028655982, 165.195195195, 75.1006770184, 109.513216374, 73.8302050793, 140.386432749,
#      75.7222681765, 172.735615616, 70.5576220463, 87.8767251462, 81.9674261848, 93.7974269006, 97.3421115625,
#      159.81021021, 62.7934027369, 107.862222222, 74.5825745901]), np.array(
#     [116.5009240009, 91.8621975469, 165.318038686, 75.386224561, 109.46996997, 73.5267583763, 140.185608686,
#      74.0129635393, 172.852789024, 69.453374928, 70.1566181566, 73.3126692566, 93.2837837838, 97.4955859404,
#      159.114597391, 62.9252272033, 107.834141834, 74.3660283621]), np.array(
#     [88.0253036437, 81.9544348203, 161.018106163, 77.7675117545, 103.192757535, 68.9992124561, 117.334586466,
#      74.9698825529, 168.941330599, 70.3549869958, 70.1363636364, 73.8823041766, 57.7966711651, 79.7510763937,
#      151.292847503, 57.0446308066, 104.136977058, 67.0719740527]), np.array(
#     [78.3096505597, 50.9252496116, 60.8230939532, 45.6829035586, 51.7446764947, 43.0073250168, 114.84413039,
#      52.5685893506, 81.4287706089, 19.0001281366, 60.7312426931, 20.0191155751, 107.171375921, 51.7685316365,
#      72.2564687976, 52.1110910808, 63.8632951133, 45.6537148878]), np.array(
#     [87.3819675772, 81.3787639595, 154.14290473, 80.8247179405, 98.7472795914, 68.8688651511, 118.338394062,
#      74.9660942795, 168.990665767, 70.439670739, 72.3367071525, 74.5223098306, 58.8819675772, 80.1434475719,
#      151.344659116, 57.0512723512, 102.170997113, 68.8895170795])
#
# print(euclidSimilar(x, y))
# print(euclidSimilar(x, m))
# print(euclidSimilar(x, z))
# print(euclidSimilar(k, z))
# print(pearsonSimilar(x, y))
# print(pearsonSimilar(x, m))
# print(pearsonSimilar(x, z))
# print(pearsonSimilar(k, z))
#
# a = np.array([4, 5, 6, 7, 8])
# print(a / 2)

import numpy as np


def window_stack(a, stepsize=1, width=7):
    print(width - (int(width / 2) + 1))
    a = np.hstack([np.zeros((width - (int(width / 2) + 1))), a, np.zeros((width - (int(width / 2) + 1)))])
    print(a)
    print(np.hstack(a[i: i + width or None:stepsize] for i in range(0, a.shape[0] - 1)))
    tmpList = list([a[i: i + width or None:stepsize] for i in range(0, a.shape[0] - 1)])
    print(tmpList)
    meanList = []
    for tmp in tmpList:
        if len(tmp) == width:
            nonzero_index = np.nonzero(tmp)
            print(nonzero_index)
            print(nonzero_index[0])
            print(tmp[nonzero_index[0][0]: (nonzero_index[0][-1] + 1)])
            meanList.append(tmp[nonzero_index[0][0]: (nonzero_index[0][-1] + 1)].mean())
    return np.hstack(np.array(meanList))


print(window_stack(np.array([1, 2, 3, 4, 5, 6, 7, 8, 9])))
