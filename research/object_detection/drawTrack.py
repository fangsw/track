import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageDraw, ImageFont
from moviepy.editor import VideoFileClip

# def window_stack(a):
#     width = max(int(a.shape[0] / 10), 2)
#     a = np.hstack([np.zeros((width - 1)), a, np.zeros((width - 1))])
#     tmpList = list([a[i: i + width or None: 1] for i in range(0, a.shape[0] - 1)])
#     meanList = []
#     for tmp in tmpList:
#         if len(tmp) == width:
#             nonzero_index = np.nonzero(tmp)
#             meanList.append(tmp[nonzero_index[0][0]: (nonzero_index[0][-1] + 1)].mean())
#     return np.hstack(np.array(meanList))

# datas = np.load("trackFileByPixel.npy")
datas = np.load("trackFileByPixel_smooth.npy")
print(datas.shape)

# for track in datas:
#     x_list = []
#     y_list = []
#     for point in track:
#         # x_list.append(float(point[2]) - float(point[4]) / 2)
#         # y_list.append(float(point[3]) - float(point[5]) / 2)
#         x_list.append(float(point[2]))
#         y_list.append(float(point[3]))
#     # x_arry = window_stack(np.array(x_list))
#     # y_array = window_stack(np.array(y_list))
#
#     # x = x_arry
#     # y = y_array
#
#     x = np.array(x_list)
#     y = np.array(y_list)
#
#     print(x)
#     print(y)
#
#     fig = plt.figure()
#     ax1 = fig.add_subplot(111)
#     ax1.set_title('Scatter Plot')
#     plt.xlabel('X')
#     plt.ylabel('Y')
#     ax1.scatter(x, y, c='r', marker='o')
#     plt.legend('x1')
#     plt.show()


#     image = Image.open("D:\\work\\BMW\\detection\\models\\research\\object_detection\\test_images\\MapbarCamera_3.png")
#     draw = ImageDraw.Draw(image)
#     for i in range(0, x.shape[0] - 1):
#         draw.line([x[i], y[i], x[i + 1], y[i + 1]], fill=(255, 0, 0), width=3)
#     image.show()

frame_num = 0


def process_image(gf, t):
    global frame_num
    image = gf(t)
    image_pil = Image.fromarray(np.uint8(image)).convert('RGB')
    draw = ImageDraw.Draw(image_pil)
    for i, track in enumerate(datas):
        for t in track:
            if int(t[7]) == frame_num:
                x = float(t[2])
                y = float(t[3])
                x1 = x + float(t[4])
                y1 = y - float(t[5])
                # print([x, y, x1, y1])
                draw.line([x, y, x, y1], fill=(255, 0, 0), width=3)
                draw.line([x, y, x1, y], fill=(255, 0, 0), width=3)
                draw.line([x1, y, x1, y1], fill=(255, 0, 0), width=3)
                draw.line([x, y1, x1, y1], fill=(255, 0, 0), width=3)
                fontSize = min(30, 30)
                myFont = ImageFont.truetype("C:\\Windows\\Fonts\\微软雅黑\\msyhbd.ttf", fontSize)
                draw.text([x, y], str(i), fill=(255, 0, 0), font=myFont)
    frame_num += 1
    return np.array(image_pil)


white_output = './test_images/track.mp4'
clip1 = VideoFileClip("test_images/out_60_10.mp4")
white_clip = clip1.fl(process_image)
white_clip.write_videofile(white_output, audio=False, preset="ultrafast", fps=10)
