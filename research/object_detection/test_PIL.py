from PIL import Image, ImageDraw, ImageFont

sourceFileName = "D:\\work\\BMW\\detection\\models\\research\\object_detection\\test_images\\MapbarCamera_3.png"
avatar = Image.open(sourceFileName)
drawAvatar = ImageDraw.Draw(avatar)

xSize, ySize = avatar.size

# drawAvatar.arc([0, 0, xSize, ySize], 0, 360, fill=(255, 0, 0))
# for i in range(0, 10):
#     drawAvatar.line([10 * i, 100, 1000, 500], fill=(255, 0, 0), width=3)
# drawAvatar.text([100, 100], "3", fill=(255, 0, 0))

fontSize = min(20, 20)  #11
myFont = ImageFont.truetype("C:\\Windows\\Fonts\\微软雅黑\\msyhbd.ttf", fontSize)
print([0.9 * xSize, 0.1 * ySize - fontSize])
drawAvatar.text([100, 150], "3", fill=(255, 0, 0), font=myFont)

avatar.show()
