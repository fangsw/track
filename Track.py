# coding=utf-8

import sys

import cv2

if __name__ == '__main__':

    # Set up tracker.
    # Instead of MIL, you can also use
    # BOOSTING, KCF, TLD, MEDIANFLOW or GOTURN

    tracker = cv2.Tracker_create("KCF")
    tracker1 = cv2.Tracker_create("KCF")

    # Read video
    video = cv2.VideoCapture("/data1/fangsw/2017-09-22/tmp_20170925/1103-活 迎旭连心口西信_CVR_1505209804_890CF767/1103-活 迎旭连心口西信_CVR_15F28018_1505209804_11.mp4")
    # video = cv2.VideoCapture("out_11.mp4")

    # Exit if video not opened.
    if not video.isOpened():
        print("Could not open video")
        sys.exit()

    # Read first frame.
    ok, frame = video.read()

    print("type(frame): {}".format(type(frame)))
    print("frame: {}".format(frame.shape))

    if not ok:
        print('Cannot read video file')
        sys.exit()

    # Define an initial bounding box
    bbox = (1726.0, 401.0, 152.0, 62.0)
    bbox1 = (20, 270, 15, 50)

    # 40.284984111785889, 77.761409282684326, 339.38950896263123, 393.69371652603149

    # Uncomment the line below to select a different bounding box
    # bbox = cv2.selectROI(frame, False)

    # Initialize tracker with first frame and bounding box
    ok = tracker.init(frame, bbox)
    ok = tracker1.init(frame, bbox1)

    while True:
        # Read a new frame
        ok, frame = video.read()
        if not ok:
            break

        # Update tracker
        ok, bbox = tracker.update(frame)
        ok, bbox1 = tracker1.update(frame)
        print(bbox)
        print(bbox1)

        # Draw bounding box
        if ok:
            p1 = (int(bbox[0]), int(bbox[1]))
            p2 = (int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3]))
            cv2.rectangle(frame, p1, p2, (0, 0, 255))

        if ok:
            p1 = (int(bbox1[0]), int(bbox1[1]))
            p2 = (int(bbox1[0] + bbox1[2]), int(bbox1[1] + bbox1[3]))
            cv2.rectangle(frame, p1, p2, (100, 200, 255))

        # Display result
        cv2.imshow("Tracking", frame)

        # Exit if ESC pressed
        k = cv2.waitKey(1) & 0xff
        if k == 27: break
