#! /usr/bin/env python
# -*- coding:utf-8 -*-

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

learning_rate = 0.1
training_epochs = 1000
rng = np.random.RandomState(1)


def fun(x):
    a0, a1, a2, a3, e = 0.1, -0.02, 0.03, -0.04, 0.05
    y = a0 + a1 * x + a2 * (x ** 2) + a3 * (x ** 3) + e
    y += 0.03 * rng.rand(1)
    return y


trX = np.array([1166., 1169., 1172., 1176., 1182., 1183., 1188.
                 , 1188., 1190., 1190., 1193., 1193., 1193., 1193., 1195., 1196., 1195.
                 , 1197., 1197., 1198., 1198., 1197., 1199., 1200., 1203., 1200., 1202.
                 , 1203., 1202., 1203., 1203., 1205., 1205., 1205., 1205., 1207., 1206.
                 , 1208., 1207., 1207., 1209., 1211., 1213., 1214., 1216., 1219., 1223.
                 , 1224., 1227., 1230., 1234., 1238., 1243., 1246., 1253., 1259., 1266.
                 , 1271., 1277., 1285., 1295., 1301., 1310., 1319., 1330., 1338., 1347.
                 , 1353., 1368., 1378., 1393., 1406., 1421., 1428., 1449., 1465., 1477.
                 , 1490., 1513., 1528.])

num_coeffs = 3
trY = np.array([1060., 1039., 1001., 974., 946., 928., 900.
                 , 883., 861., 846., 820., 805., 788., 776., 760., 747., 734.
                 , 725., 712., 701., 691., 688., 681., 675., 660., 655., 645.
                 , 641., 630., 625., 619., 614., 612., 598., 595., 590., 583.
                 , 578., 569., 565., 557., 554., 550., 546., 539., 536., 530.
                 , 525., 520., 516., 510., 507., 508., 502., 495., 494., 486.
                 , 486., 479., 474., 473., 471., 468., 464., 460., 456., 450.
                 , 448., 444., 445., 445., 444., 442., 439., 433., 434., 433.
                 , 428., 426., 423.]).reshape(-1, 1)

X = tf.placeholder("float")
Y = tf.placeholder("float")


def model(X, w):
    terms = []
    for i in range(num_coeffs):
        term = tf.multiply(w[i], tf.pow(X, i))
        terms.append(term)
    return tf.add_n(terms)


w = tf.Variable([0.] * num_coeffs, name="parameters")
y_model = model(X, w)

cost = tf.reduce_sum(tf.square(Y - y_model))
train_op = tf.train.AdamOptimizer(learning_rate).minimize(cost)

with tf.Session() as sess:
    init = tf.global_variables_initializer()
    sess.run(init)

    for epoch in range(training_epochs):
        for (x, y) in zip(trX, trY):
            sess.run(train_op, feed_dict={X: x, Y: y})
            print(sess.run(w))

    w_val = sess.run(w)
    print(w_val)

plt.figure()
plt.xlabel('x')
plt.ylabel('y')
plt.grid(True)
plt.title('polynomial regression(tensorflow)')
plt.scatter(trX, trY)
trX2 = trX
trY2 = 0
for i in range(num_coeffs):
    trY2 += w_val[i] * np.power(trX2, i)
print(trY2)
plt.plot(trX2, trY2, 'r-')
plt.show()
