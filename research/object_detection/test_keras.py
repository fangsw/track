import keras as k
import numpy as np

np.random.seed(1337)
from keras.models import Sequential
from keras.layers import Dense
import matplotlib.pyplot as plt
from sklearn.preprocessing import scale


X = np.array([1166.,1169.,1172.,1176.,1182.,1183.,1188.
,1188.,1190.,1190.,1193.,1193.,1193.,1193.,1195.,1196.,1195.
,1197.,1197.,1198.,1198.,1197.,1199.,1200.,1203.,1200.,1202.
,1203.,1202.,1203.,1203.,1205.,1205.,1205.,1205.,1207.,1206.
,1208.,1207.,1207.,1209.,1211.,1213.,1214.,1216.,1219.,1223.
,1224.,1227.,1230.,1234.,1238.,1243.,1246.,1253.,1259.,1266.
,1271.,1277.,1285.,1295.,1301.,1310.,1319.,1330.,1338.,1347.
,1353.,1368.,1378.,1393.,1406.,1421.,1428.,1449.,1465.,1477.
,1490.,1513.,1528.])
X = scale(X)
Y = np.array([1060.,1039.,1001., 974., 946., 928., 900.
, 883., 861., 846., 820., 805., 788., 776., 760., 747., 734.
, 725., 712., 701., 691., 688., 681., 675., 660., 655., 645.
, 641., 630., 625., 619., 614., 612., 598., 595., 590., 583.
, 578., 569., 565., 557., 554., 550., 546., 539., 536., 530.
, 525., 520., 516., 510., 507., 508., 502., 495., 494., 486.
, 486., 479., 474., 473., 471., 468., 464., 460., 456., 450.
, 448., 444., 445., 445., 444., 442., 439., 433., 434., 433.
, 428., 426., 423.])
Y = scale(Y)

X_train, Y_train = X, Y
X_test, Y_test = X, Y

model = Sequential()
model.add(Dense(input_dim=1, units=30, activation=k.activations.sigmoid))
model.add(Dense(input_dim=30, output_dim=1))

model.summary()

model.compile(loss='mse', optimizer=k.optimizers.adamax())

print('Training -----------')
for step in range(3000):
    cost = model.train_on_batch(X_train, Y_train)
    if step % 100 == 0:
        print('train cost: ', cost)

# model.fit(X_train, Y_train, epochs=3000)

print('\nTesting ------------')
cost = model.evaluate(X_test, Y_test, batch_size=10)
print('test cost:', cost)
# W, b = model.layers[0].get_weights()
# print('Weights=', W, '\nbiases=', b)

Y_pred = model.predict(X_test)
plt.scatter(X_test, Y_test)
plt.plot(X_test, Y_pred)
plt.show()
