# coding=utf-8

import csv
import numpy as np
from numpy import linalg as la
import Smooth_track as st

header = ['AgentID', 'AgentName', 'Birth', 'Timestamp', 'TypeProbabilityCar', 'TypeProbabilityTruck',
          'TypeProbabilityBus',
          'TypeProbabilityPedestrian', 'TypeProbabilityScooter', 'TypeProbabilityBike', 'TypeProbabilityUnknown',
          'ObjectWidth',
          'ObjectHeight', 'ObjectLength', 'PositionRelX', 'PositionRelY', 'PositionRelZ', 'PositionAbsX',
          'PositionAbsY',
          'PositionAbsZ', 'VelocityAbs', 'VelocityRelX', 'VelocityRelY', 'VelocityLateral', 'AccelerationAbs',
          'AccelerationRel',
          'YawAbs', 'YawRel', 'YawRelToLane', 'LaneRelNr', 'LaneWidth', 'DistanceToStopLine', 'DistanceToNextSection',
          'VerticalDistToCenterLine', 'LaneType', 'TrafficLight', 'TurnLeftTrace', 'GoStraightTrace']


fps = 10
FRAME_THRESHOLD = 3 * fps  # n秒
TAKE_FRAME_INTERVAL = 1
THRESHOLD = 0.5
PXIEL_THRESHOLD = 0.8
IMG_SIZE = (1920, 1080)


def calcIOU(box1, box2):
    one_x, one_y, one_w, one_h = box1
    two_x, two_y, two_w, two_h = box2

    one_x, one_y, one_w, one_h = float(one_x), float(one_y), float(one_w), float(one_h)
    two_x, two_y, two_w, two_h = float(two_x), float(two_y), float(two_w), float(two_h)

    if (abs(one_x - two_x) < ((one_w + two_w) / 2.0)) and (abs(one_y - two_y) < ((one_h + two_h) / 2.0)):
        lu_x_inter = max((one_x - (one_w / 2.0)), (two_x - (two_w / 2.0)))
        lu_y_inter = min((one_y + (one_h / 2.0)), (two_y + (two_h / 2.0)))

        rd_x_inter = min((one_x + (one_w / 2.0)), (two_x + (two_w / 2.0)))
        rd_y_inter = max((one_y - (one_h / 2.0)), (two_y - (two_h / 2.0)))

        inter_w = abs(rd_x_inter - lu_x_inter)
        inter_h = abs(lu_y_inter - rd_y_inter)

        inter_square = inter_w * inter_h
        union_square = (one_w * one_h) + (two_w * two_h) - inter_square

        calcIOU = inter_square / union_square * 1.0
        # print("calcIOU:", calcIOU)
    else:
        calcIOU = 0

    return calcIOU


def export_data(list, path):
    write_list = []
    for i, track in enumerate(list):
        for t in track:
            classes = t[0]
            classes_v = [0, 0, 0, 0, 0, 0, 0]
            if classes == 'car':
                classes_v[0] = float(t[1])
            elif classes == 'truck':
                classes_v[1] = float(t[1])
            elif classes == 'bus':
                classes_v[2] = float(t[1])
            elif classes == 'person':
                classes_v[3] = float(t[1])
            elif classes == 'motorcycle':
                classes_v[4] = float(t[1])
            elif classes == 'bicycle':
                classes_v[5] = float(t[1])
            else:
                classes_v[6] = float(t[1])
            write_list.append([i, 'objectcar', int(track[0][7]), int(t[7]), classes_v[0], classes_v[1], classes_v[2], classes_v[3],
                 classes_v[4], classes_v[5], classes_v[6], -1, -1, -1, -1, -1, -1, t[2], t[3][0], 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1])
    with open(path, 'w', newline='') as f:
        w = csv.writer(f)
        w.writerow(header)
        for l in write_list:
            w.writerow(l)


def readCVS(path):
    results = []
    with open(path) as f:
        f_csv = csv.reader(f)
        next(f_csv)
        for i, row in enumerate(f_csv):
            # if (i + 1) % 10 == 0:
            #     break
            frame_num = row[0]
            list = row[1:]
            num = round(len(list) / 7)
            row_list = []
            for ii in range(num):
                start = ii * 7
                end = start + 7
                sublist = list[start: end]
                if sublist[0] is not "":
                    sublist.append(frame_num)
                    row_list.append(sublist)
            results.append(row_list)
    return results


def boxTake(list):
    return list[2:6]


def pxielTake(list):
    pxielList = list[6].split("|")
    return np.array(pxielList, dtype=np.float32)


def euclidSimilar(v1, v2):
    return 1.0 - 1.0 / (1.0 + la.norm(v1 - v2))


def pearsonSimilar(v1, v2):
    return np.corrcoef(v1, v2, rowvar=0)[0][1] * 0.5 + 0.5


# 过滤轨迹根据 轨迹像素和轨迹点的类型
def filterTrack(list, pixel_item, track_sum_item):
    print('filterTrack start !!! track x or y length > {}, track point num > {}'.format(pixel_item, track_sum_item))
    results = []
    classes_list = []
    for track in list:
        classes_map = {}
        for t in track:
            if t[0] in classes_map.keys():
                classes_map[t[0]] += 1
            else:
                classes_map[t[0]] = 1
        sort_classes = sorted(classes_map.items(), key=lambda classes_map: classes_map[1])
        max_classes = sort_classes[-1][0]
        classes_list.append(max_classes)

    for i, track in enumerate(list):
        classes_filter_track = []
        for t in track:
            classes = t[0]
            if classes == classes_list[i]:
                classes_filter_track.append(t)
        start = classes_filter_track[0]
        end = classes_filter_track[-1]
        start_x = float(start[2])
        start_y = float(start[3])
        end_x = float(end[2])
        end_y = float(end[3])
        if (abs(start_x - end_x) > pixel_item or abs(start_y - end_y) > pixel_item) and len(track) > track_sum_item:
            results.append(classes_filter_track)

    print('filterTrack end, track num: {}'.format(len(results)))
    return results


def mergeTrack(list):
    print('mergeTrack Start !!!')
    mergeInfo_list = []
    result_list = []
    for track in list:
        start_frame_num = track[0][7]
        end_frame_num = track[-1][7]
        track_count = len(track)
        pxiel_info = np.array(track[0][6].split('|'), dtype=np.float32)
        # print(pxiel_info)
        for i, t in enumerate(track):
            if i > 0:
                pxiel_info += np.array(t[6].split('|'), dtype=np.float32)
                # print(pxiel_info)
        mergeInfo_list.append([start_frame_num, end_frame_num, pxiel_info / track_count])
    exclude_list = []
    for i, mergeInfo in enumerate(mergeInfo_list):
        pxiel_info = mergeInfo[2]
        start_frame_num = mergeInfo[0]
        end_frame_num = mergeInfo[1]
        score_pxiel = []
        for mergeInfo1 in mergeInfo_list:
            score_pxiel.append(pearsonSimilar(pxiel_info, mergeInfo1[2]))
        score_pxiel[i] = -99
        score_pxiel = np.array(score_pxiel)
        if score_pxiel.max() > 0.9:
            if not i in exclude_list:
                index = score_pxiel.argmax()
                m = mergeInfo_list[index]
                start_frame_num1 = m[0]
                end_frame_num1 = m[1]
                if start_frame_num1 > end_frame_num and end_frame_num1 > end_frame_num:
                    c = list[i] + list[index]
                    if not c in result_list:
                        result_list.append(c)
                        exclude_list.append(index)
                elif start_frame_num > end_frame_num1 and end_frame_num > end_frame_num1:
                    c = list[index] + list[i]
                    if not c in result_list:
                        result_list.append(c)
                        exclude_list.append(index)
        else:
            result_list.append(list[i])
    print('mergeTrack end, track num: {}'.format(len(result_list)))
    return result_list


# 过滤视频每帧图片的边框
def filterImgPxiel(box, filter_pxiel, img_size):
    width, high = img_size
    return (width - filter_pxiel) > float(box[0]) > filter_pxiel and (high - filter_pxiel) > float(
        box[1]) > filter_pxiel and float(box[2]) > 0 and float(box[3]) > 0


def build_track(input_path, output_path):
    print("build track start !!!")
    data_list = readCVS(input_path)
    frist_line = data_list[0]
    tail_line = data_list[1:]
    track_list = []
    for bbox_info in frist_line:
        box = boxTake(bbox_info)
        if filterImgPxiel(box, 10, IMG_SIZE):
            track_list.append([bbox_info])
    for i, line in enumerate(tail_line[::TAKE_FRAME_INTERVAL]):
        print("process frame: {}".format(i * TAKE_FRAME_INTERVAL + 1))

        for bbox_info in line:
            box = boxTake(bbox_info)
            pxiel = pxielTake(bbox_info)
            if filterImgPxiel(box, 10, IMG_SIZE):
                frame_num = int(bbox_info[-1])
                iou_list = []
                frame_interval_list = []
                pxiel_dist_list = []

                for t in track_list:
                    last_track_box = boxTake(t[-1])
                    last_track_pxiel = pxielTake(t[-1])
                    last_track_frame_num = int(t[-1][-1])
                    iou = calcIOU(box, last_track_box)
                    iou_list.append(iou)
                    frame_interval_list.append(abs(frame_num - last_track_frame_num))
                    pxiel_dist_list.append(pearsonSimilar(pxiel, last_track_pxiel))

                score_list = (np.array(iou_list) + np.array(pxiel_dist_list)) / 2

                sub_pxiel_dist = []
                for vi, val in enumerate(score_list):
                    if val > THRESHOLD:
                        sub_pxiel_dist.append(pxiel_dist_list[vi])

                if len(sub_pxiel_dist) > 0:
                    pxiel_dist = np.array(sub_pxiel_dist).max()
                    index = pxiel_dist_list.index(pxiel_dist)
                    frame_interval = frame_interval_list[index]

                    if FRAME_THRESHOLD > frame_interval > 0 and pxiel_dist > PXIEL_THRESHOLD:
                        track_list[index].append(bbox_info)
                    else:
                        track_list.append([bbox_info])
                else:
                    track_list.append([bbox_info])

    track_array = np.asarray(track_list)
    np.save(output_path, track_array)
    print(output_path + ' created !!!')

    print('build track end !!! track_list num: {}'.format(len(track_list)))
    return track_list


if __name__ == "__main__":

    # track_list = build_track("boxFileByPixel.csv", output_path="trackFileByPixel.npy")
    track_list = np.load("trackFileByPixel_smooth.npy")
    # track_list = mergeTrack(track_list)
    # 过滤移动像素x轴方向和y轴方向小于 n 的轨迹, 轨迹点数量大于n个
    # track_list = filterTrack(track_list, 10, 2)
    # track_list = st.smooth(track_list, 1, output_path='trackFileByPixel_smooth.npy')
    export_data(track_list, path='track.csv')
