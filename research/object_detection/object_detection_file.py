# coding=utf-8

import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile

from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image

from moviepy.editor import VideoFileClip

import time

sys.path.append("..")

from utils import label_map_util
from utils import visualization_utils as vis_util
import track.TrackByDetection as tbd

config = tf.ConfigProto()
config.gpu_options.allow_growth = True

MODEL_NAME = 'faster_rcnn_inception_resnet_v2_atrous_coco_11_06_2017'

PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'

PATH_TO_LABELS = os.path.join('data', 'mscoco_label_map.pbtxt')

NUM_CLASSES = 90

file_list = []
frame_num = 0
cut_x_count = 3
cut_y_count = 3

detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES,
                                                            use_display_name=True)
category_index = label_map_util.create_category_index(categories)


def detect_objects(image_np, sess, min_score_thresh):
    global frame_num
    frame_num += 1
    image_np_expanded = np.expand_dims(image_np, axis=0)

    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
    boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
    scores = detection_graph.get_tensor_by_name('detection_scores:0')
    classes = detection_graph.get_tensor_by_name('detection_classes:0')
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')

    # startTime = time.time()
    (boxes, scores, classes, num_detections) = sess.run([boxes, scores, classes, num_detections],
                                                        feed_dict={image_tensor: image_np_expanded})
    # print("cost time: {}".format(time.time() - startTime))
    # print("fram_num: {}".format(frame_num))

    line = str(frame_num)

    image_pil = Image.fromarray(np.uint8(image_np)).convert('RGB')
    (im_width, im_height) = image_pil.size
    s = np.squeeze(scores)
    b = np.squeeze(boxes)
    c = np.squeeze(classes)
    for i in range(s.shape[0]):
        if s[i] > min_score_thresh:
            ymin, xmin, ymax, xmax = b[i]
            rxmin, rxmax, rymin, rymax = xmin * im_width, xmax * im_width, ymin * im_height, ymax * im_height
            # print("source: {}, class: {}, boxes: {}".format(s[i], category_index[c[i]]['name'], (rxmin, rxmax, rymin, rymax)))
            box_c = boxConvert(rxmin, rxmax, rymin, rymax)
            # print("box_c: {}".format(box_c))
            line += "," + category_index[c[i]]['name'] + "," + str(s[i]) + "," + str(box_c[0]) + "," + str(
                box_c[1]) + "," + str(box_c[2]) + "," + str(box_c[3]) + ","
            try:
                # bbox框出的图像切割
                box_img = image_pil.crop((rxmin, rymin, rxmax, rymax))
                sub_w = round((box_c[2] / cut_x_count), 2)
                sub_h = round((box_c[3] / cut_y_count), 2)
                for ii in range(cut_x_count):
                    for jj in range(cut_y_count):
                        sub_img = box_img.crop((sub_w * ii, sub_h * jj, sub_w * (ii + 1), sub_h * (jj + 1)))
                        sub_arr = np.array(sub_img)
                        line += str(sub_arr.mean()) + ";" + str(sub_arr.std()) + "|"
            except Exception:
                line = "|"
            line = line[:-1]
    file_list.append(line)

    vis_util.visualize_boxes_and_labels_on_image_array(
        image_np,
        np.squeeze(boxes),
        np.squeeze(classes).astype(np.int32),
        np.squeeze(scores),
        category_index,
        min_score_thresh=min_score_thresh,
        use_normalized_coordinates=True,
        line_thickness=4)
    return image_np


def boxConvert(xmin, xmax, ymin, ymax):
    return round(xmin), round(ymax), round(abs(xmin - xmax)), round(abs(ymin - ymax))


def process_image1(gf, t):
    image = gf(t)
    with detection_graph.as_default():
        with tf.Session(graph=detection_graph, config=config) as sess:
            image_process = detect_objects(image, sess, 0.5)
            return image_process


white_output = './test_images/out_60_10.mp4'
clip1 = VideoFileClip("/data1/fangsw/2017-09-22/tmp_20170925/2502-活 迎旭昌平口东警1_CVR_1505209804_D03D69D8/2502-活 迎旭昌平口东警1_CVR_164D5D80_1505209804_11.mp4").subclip(0, 60)
# clip1 = VideoFileClip("D:\work\BMW\detection\download.mp4").subclip(0, 5)
white_clip = clip1.fl(process_image1)
white_clip.write_videofile(white_output, audio=False, preset="ultrafast", fps=10)

with open("boxFileByPixel.csv", "w") as f:
    for line in file_list:
        f.write(line + "\n")
    f.close()
