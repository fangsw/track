# coding=utf-8

import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile

from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image

from moviepy.editor import VideoFileClip

import time

# This is needed to display the images.
# %matplotlib inline

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")

from utils import label_map_util

from utils import visualization_utils as vis_util

# What model to download.
MODEL_NAME = 'faster_rcnn_inception_resnet_v2_atrous_coco_11_06_2017'
MODEL_FILE = MODEL_NAME + '.tar.gz'
DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'

# Path to frozen detection graph. This is the actual model that is used for the object detection.
PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'

# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = os.path.join('data', 'mscoco_label_map.pbtxt')

NUM_CLASSES = 90

detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)


def detect_objects(image_np, sess, detection_graph, min_score_thresh=0.5):
    # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
    image_np_expanded = np.expand_dims(image_np, axis=0)
    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

    # Each box represents a part of the image where a particular object was detected.
    boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

    # Each score represent how level of confidence for each of the objects.
    # Score is shown on the result image, together with the class label.
    scores = detection_graph.get_tensor_by_name('detection_scores:0')
    classes = detection_graph.get_tensor_by_name('detection_classes:0')
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')

    startTime = time.time()
    # Actual detection.
    (boxes, scores, classes, num_detections) = sess.run([boxes, scores, classes, num_detections],
                                                        feed_dict={image_tensor: image_np_expanded})
    print("cost time: {}".format(time.time() - startTime))

    # Print for testing
    # det_class = np.squeeze(classes).astype(np.int32)
    # det_score = np.squeeze(scores)
    # for x in range(0, int(np.squeeze(num_detections))):
    #     if det_score[x] < 0.5:
    #         print("det", x, det_class[x], det_score[x])

    # print("scores: {}".format(scores))

    image_pil = Image.fromarray(np.uint8(image_np)).convert('RGB')
    (im_width, im_height) = image_pil.size
    # print("width: {} height: {}".format(im_width, im_height))
    # (left, right, top, bottom) = (xmin * im_width, xmax * im_width, ymin * im_height, ymax * im_height)
    # print("boxes dim: {} boxes value: {}".format(np.squeeze(boxes).shape,
    #                                              [(xmin * im_width, xmax * im_width, ymin * im_height, ymax * im_height) for (ymin, xmin, ymax, xmax) in np.squeeze(boxes)]))
    s = np.squeeze(scores)
    b = np.squeeze(boxes)
    c = np.squeeze(classes)
    for i in range(s.shape[0]):
        if s[i] > min_score_thresh:
            ymin, xmin, ymax, xmax = b[i]
            print("source: {}, class: {}, boxes: {}".format(s[i], category_index[c[i]]['name'], (xmin * im_width, xmax * im_width, ymin * im_height, ymax * im_height)))

    # Visualization of the results of a detection.
    vis_util.visualize_boxes_and_labels_on_image_array(
        image_np,
        np.squeeze(boxes),
        np.squeeze(classes).astype(np.int32),
        np.squeeze(scores),
        category_index,
        min_score_thresh=min_score_thresh,
        use_normalized_coordinates=True,
        line_thickness=4)
    return image_np


# def load_image_into_numpy_array(image):
#     (im_width, im_height) = image.size
#     return np.array(image.getdata()).reshape((im_height, im_width, 3)).astype(np.uint8)


# For the sake of simplicity we will use only 2 images:
# image1.jpg
# image2.jpg
# If you want to test the code with your images, just add path to the images to the TEST_IMAGE_PATHS.
# PATH_TO_TEST_IMAGES_DIR = 'test_images'
# TEST_IMAGE_PATHS = [os.path.join(PATH_TO_TEST_IMAGES_DIR, 'MapbarCamera_{}.png'.format(i)) for i in range(3, 4)]
#
# # Size, in inches, of the output images.
# IMAGE_SIZE = (12, 8)

# def load_image_into_numpy_array(image):
#     (im_width, im_height) = image.size
#     return np.array(image.getdata()).reshape((im_height, im_width, 3)).astype(np.uint8)


# from PIL import Image
#
# for image_path in TEST_IMAGE_PATHS:
#     image = Image.open(image_path)
#     image_np = load_image_into_numpy_array(image)
#     # plt.imshow(image_np)
#     print(image.size, image_np.shape)


# # Load a frozen TF model
# detection_graph = tf.Graph()
# with detection_graph.as_default():
#     od_graph_def = tf.GraphDef()
#     with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
#         serialized_graph = fid.read()
#         od_graph_def.ParseFromString(serialized_graph)
#         tf.import_graph_def(od_graph_def, name='')

def process_image(image):
    with detection_graph.as_default():
        with tf.Session(graph=detection_graph) as sess:
            image_process = detect_objects(image, sess, detection_graph)
            return image_process


with detection_graph.as_default():
    with tf.Session(graph=detection_graph) as sess:
        # for image_path in TEST_IMAGE_PATHS:
        #     image = Image.open(image_path)
        #     image_np = load_image_into_numpy_array(image)
        #     image_process = detect_objects(image_np, sess, detection_graph)
        #     print(image_process.shape)
        #     plt.figure(figsize=IMAGE_SIZE)
        #     plt.imshow(image_process)
        #     plt.show()
        white_output = './test_images/out_11.mp4'
        # clip1 = VideoFileClip(
        #     "/data1/fangsw/2017-09-22/tmp_20170925/1103-活 迎旭连心口西信_CVR_"
        #     "1505209804_890CF767/1103-活 迎旭连心口西信_CVR_15F28018_1505209804_11.mp4").subclip(0, 10)
        clip1 = VideoFileClip("D:\work\BMW\detection\download.mp4").subclip(0, 1)
        white_clip = clip1.fl_image(process_image)  # NOTE: this function expects color images!!s
        white_clip.write_videofile(white_output, audio=False, threads=10, preset="ultrafast", fps=1)
