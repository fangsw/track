import numpy as np

# a = [1, 2, 3, 4]
# b = [1, 2, 3, 4, 5]
# d = a + b
# # a += b
# # print(a.pop(1))
# # for i, v in enumerate(a):
# #     print(i, a.pop(i))
#
# c = [a, b]
# e = c.copy()
# print(e.remove(a))
# print(c.pop(0))
# print(c)
# print(e)
# print(d)
# print(not d in c)
# c.append(d)
# print(not d in c)

import csv

csvfile = open('csvtest.csv', 'w', newline='')
writer = csv.writer(csvfile)
writer.writerow(['id', 'url', 'keywords'])
data = [
    ('1', 'http://www.xiaoheiseo.com/', 'dd'),
    ('2', 'http://www.baidu.com/', 'd'),
    ('3', 'http://www.jd.com/', 'fff')
]
writer.writerows(data)
csvfile.close()
