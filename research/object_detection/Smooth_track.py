# coding=utf-8

import keras as k
import numpy as np

np.random.seed(1337)
from keras.models import Sequential
from keras.layers import Dense
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler


def smooth(track_list, cost_threshold, output_path):
    print('smooth start !!! cost > {}'.format(cost_threshold))
    smooth_filter_list = []
    for list in track_list:
        x = []
        y = []
        for track_point in list:
            x.append(float(track_point[2]))
            y.append(float(track_point[3]))
        X = np.array(x)
        X_scaler = StandardScaler().fit(X.reshape(-1, 1))
        X_mean = X_scaler.mean_
        X_scale = X_scaler.scale_
        X = X_scaler.transform(X.reshape(-1, 1)).reshape(-1)

        Y = np.array(y)
        Y_scaler = StandardScaler().fit(Y.reshape(-1, 1))
        Y_mean = Y_scaler.mean_
        Y_scale = Y_scaler.scale_
        Y = Y_scaler.transform(Y.reshape(-1, 1)).reshape(-1)

        X_train, Y_train = X, Y
        # X_test, Y_test = X, Y

        model = Sequential()
        model.add(k.layers.Dense(input_dim=1, units=30, activation=k.activations.sigmoid))
        model.add(Dense(units=1))

        # model.summary()

        model.compile(loss='mse', optimizer=k.optimizers.adamax())

        # print('Training -----------')
        for step in range(3000):
            model.train_on_batch(X_train, Y_train)
            # cost = model.train_on_batch(X_train, Y_train)
            # if step % 100 == 0:
            #     print('train cost: ', cost)

        # model.fit(X_train, Y_train, epochs=3000)

        # print('\nTesting ------------')
        cost = model.evaluate(X_train, Y_train, batch_size=10)
        # print('test cost:', cost)
        # W, b = model.layers[0].get_weights()
        # print('Weights=', W, '\nbiases=', b)

        # Y_pred = model.predict(X_test)
        # plt.scatter(X_test, Y_test)
        # plt.plot(X_test, Y_pred)
        # plt.show()
        if cost < cost_threshold:
            Y_pre = model.predict(X_train)
            X_train_real = X_train * X_scale + X_mean
            Y_pre_real = Y_pre * Y_scale + Y_mean
            for i, point in enumerate(list):
                point[2] = X_train_real[i]
                point[3] = Y_pre_real[i]
            smooth_filter_list.append(list)

    smoooth_array = np.asarray(smooth_filter_list)
    np.save(output_path, smoooth_array)
    print(output_path + ' created !!!')

    print('smooth end, track num: {}'.format(len(smooth_filter_list)))
    return smooth_filter_list
