# 导入pillow
from PIL import Image
import numpy as np

a = "abcd"
print(a[:-1])

# 加载原始图片
img = Image.open("D:\\work\\BMW\\detection\\models\\research\\object_detection\\test_images\\MapbarCamera_3.png")

# 从左上角开始 剪切 200*200的图片
x1, y1, x2, y2 = 100, 100, 200, 200
img1 = img.crop((x1, y1, x2, y2))

sub_w = round(100 / 3, 2)
sub_h = round(100 / 3, 2)
print(sub_w, sub_h)

for i in range(3):
    for j in range(3):
        sub_img = img1.crop((sub_w * i, sub_h * j, sub_w * (i + 1), sub_h * (j + 1)))
        # sub_img.save(str(i)+str(j)+".jpg")
        print(np.array(sub_img).shape)
        print(np.array(sub_img).mean())
        print(np.array(sub_img).std())

# img1.save("lena3.jpg")
