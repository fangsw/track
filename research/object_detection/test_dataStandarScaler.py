from sklearn.preprocessing import StandardScaler
import numpy as np

X = np.array([1., -1., 2., 2., 0., 0., 100., 1., -1.])
print(X)
scaler = StandardScaler().fit(X.reshape(-1, 1))
print(scaler)
print(scaler.mean_)
print(scaler.scale_)
transform = scaler.transform(X.reshape(-1, 1)).reshape(-1)
print(transform)
print(transform * scaler.scale_ + scaler.mean_)
