# coding=utf-8

import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile

from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image

from moviepy.editor import VideoFileClip

import time

sys.path.append("..")

from utils import label_map_util
from utils import visualization_utils as vis_util
import track.TrackByDetection as tbd

config = tf.ConfigProto()
config.gpu_options.allow_growth = True

MODEL_NAME = 'faster_rcnn_inception_resnet_v2_atrous_coco_11_06_2017'
MODEL_FILE = MODEL_NAME + '.tar.gz'
DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'

PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'

PATH_TO_LABELS = os.path.join('data', 'mscoco_label_map.pbtxt')

NUM_CLASSES = 90

pixel_trajectory_list = []
frame_num = 0

detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES,
                                                            use_display_name=True)
category_index = label_map_util.create_category_index(categories)
multiTracker = tbd.init()


def detect_objects(image_np, sess, min_score_thresh):
    global frame_num
    frame_num += 1
    image_np_expanded = np.expand_dims(image_np, axis=0)

    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
    boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
    scores = detection_graph.get_tensor_by_name('detection_scores:0')
    classes = detection_graph.get_tensor_by_name('detection_classes:0')
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')

    startTime = time.time()
    (boxes, scores, classes, num_detections) = sess.run([boxes, scores, classes, num_detections],
                                                        feed_dict={image_tensor: image_np_expanded})
    print("cost time: {}".format(time.time() - startTime))

    print("fram_num: {}".format(frame_num))

    image_pil = Image.fromarray(np.uint8(image_np)).convert('RGB')
    (im_width, im_height) = image_pil.size
    s = np.squeeze(scores)
    b = np.squeeze(boxes)
    c = np.squeeze(classes)
    for i in range(s.shape[0]):
        if s[i] > min_score_thresh:
            ymin, xmin, ymax, xmax = b[i]
            print("source: {}, class: {}, boxes: {}".format(s[i], category_index[c[i]]['name'], (
                xmin * im_width, xmax * im_width, ymin * im_height, ymax * im_height)))
            box_c = boxConvert(xmin * im_width, xmax * im_width, ymin * im_height, ymax * im_height)
            print("box_c: {}".format(box_c))
            if tbd.isNewBox(pixel_trajectory_list, box_c):
                sub_trajectory_list = tbd.createTracker(box_c, image_np, multiTracker, frame_num)
                pixel_trajectory_list.append(sub_trajectory_list)
                print("trajectory count: {}".format(len(pixel_trajectory_list)))

    tbd.track(multiTracker, pixel_trajectory_list, image_np, frame_num)

    vis_util.visualize_boxes_and_labels_on_image_array(
        image_np,
        np.squeeze(boxes),
        np.squeeze(classes).astype(np.int32),
        np.squeeze(scores),
        category_index,
        min_score_thresh=min_score_thresh,
        use_normalized_coordinates=True,
        line_thickness=4)
    return image_np


def boxConvert(xmin, xmax, ymin, ymax):
    return round(xmin), round(ymax), round(abs(xmin - xmax)), round(abs(ymin - ymax))


def process_image1(gf, t):
    image = gf(t)
    with detection_graph.as_default():
        with tf.Session(graph=detection_graph, config=config) as sess:
            image_process = detect_objects(image, sess, 0.5)
            return image_process


white_output = './test_images/out_11.mp4'
clip1 = VideoFileClip(
    "/data1/fangsw/2017-09-22/tmp_20170925/1103-活 迎旭连心口西信_CVR_"
    "1505209804_890CF767/1103-活 迎旭连心口西信_CVR_15F28018_1505209804_11.mp4").subclip(0, 10)
# clip1 = VideoFileClip("D:\work\BMW\detection\download.mp4").subclip(0, 5)
white_clip = clip1.fl(process_image1)
white_clip.write_videofile(white_output, audio=False, preset="ultrafast", fps=10)
print(pixel_trajectory_list)
