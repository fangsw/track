# coding: utf-8

import sys
import cv2


def init():
    return cv2.MultiTracker("KCF")


def createTracker(box, frame, multiTracker, frame_num):
    multiTracker.add(frame, box)
    result = [(frame_num, box)]
    return result


def track(multiTracker, results, frame, frame_num):
    _, boxes = multiTracker.update(frame)
    for i, newbox in enumerate(boxes):
        result = results[i]
        result.append((frame_num, newbox))
    return results


def isNewBox(results, box):
    flag = True
    if len(results) != 0:
        for result in results:
            _, box1 = result[(len(result) - 1)]
            iou = calcIOU(box1, box)
            if iou > 0.3:
                flag = False
                break
    return flag


def calcIOU(box1, box2):
    one_x, one_y, one_w, one_h = box1
    two_x, two_y, two_w, two_h = box2
    if (abs(one_x - two_x) < ((one_w + two_w) / 2.0)) and (abs(one_y - two_y) < ((one_h + two_h) / 2.0)):
        lu_x_inter = max((one_x - (one_w / 2.0)), (two_x - (two_w / 2.0)))
        lu_y_inter = min((one_y + (one_h / 2.0)), (two_y + (two_h / 2.0)))

        rd_x_inter = min((one_x + (one_w / 2.0)), (two_x + (two_w / 2.0)))
        rd_y_inter = max((one_y - (one_h / 2.0)), (two_y - (two_h / 2.0)))

        inter_w = abs(rd_x_inter - lu_x_inter)
        inter_h = abs(lu_y_inter - rd_y_inter)

        inter_square = inter_w * inter_h
        union_square = (one_w * one_h) + (two_w * two_h) - inter_square

        calcIOU = inter_square / union_square * 1.0
        # print("calcIOU:", calcIOU)
    else:
        calcIOU = 0

    return calcIOU


# if __name__ == "__main__":
#     print(calcIOU((1, 1, 1, 1), (1.9, 1.9, 1, 1)))
